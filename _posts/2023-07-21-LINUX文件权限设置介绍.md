---
layout: post  
title: "「Linux 学习笔记 - 文件权限设置」 "  
date: 2023-07-21  
categories: TECH&LINUX  
---


Linux学习笔记 - 文件权限设置
==================
系列：「Linux 学习笔记」    
主题： **文件权限设置**    
摘自：EvanNotFound 博客  [原文快照](https://ohevan.com/linux-authorize-user-command.html)    
_2023-07-21_


[](#A-引入 "A 引入")A 引入
--------------------

搞 **WordPress** 根目录文件的时候，为了确保安全，我想改一下文件的权限（之前一直是777）。

每次都记不住linux里面文件的权限，所以写个笔记提醒自己，偶尔翻翻。

[](#B-细则 "B 细则")B 细则
--------------------

### [](#B-1-命令概念 "B.1 命令概念")B.1 命令概念

这两个可以用一张图形象地说明 【来自 [菜鸟教程](https://www.runoob.com/linux/linux-file-attr-permission.html) 】

![](https://evan.beee.top/wp-content/uploads/2022/03/image.png?x-oss-process=style/turn-to-webp)

#### [](#B-1-1-chmod "B.1.1 chmod")B.1.1 chmod

顾名思义，就是 change mod 的缩写，可以更改文件对于用户的权限，就是规定用户开门的权限

#### [](#B-1-2-chown "B.1.2 chown")B.1.2 chown

change owner 的缩写，更改文件归属的用户组，就是授权用户

### [](#B-2-文件格式 "B.2 文件格式")B.2 文件格式

![](https://evan.beee.top/wp-content/uploads/2022/03/Screen-Shot-2022-03-30-at-12.23.44.png?x-oss-process=style/turn-to-webp)

如图，文件名下方有 `drwxrwxr-x`

其中，第一个字母

*   当为 **d** 则是目录
*   当为 **-** 则是文件
*   若是 **l** 则表示为链接文档(link file)
*   若是 **b** 则表示为装置文件里面的可供储存的接口设备(可随机存取装置)
*   若是 **c** 则表示为装置文件里面的串行端口设备，例如键盘、鼠标(一次性读取装置)

接下来字符中，以三个为一组，且均为 **rwx** 的三个参数的组合。

*   **r** 代表可读(read)
*   **w** 代表可写(write)
*   **x** 代表可执行(execute)
*   没有权限，就会出现减号 **—**

![Linux chmod 权限参数](https://evan.beee.top/wp-content/uploads/2022/03/image-1.png?x-oss-process=style/turn-to-webp)

*   第 **1-3** 位确定该文件的所有者拥有该文件的权限
*   第 **4-6** 位确定所有者的同组用户拥有该文件的权限
*   第 **7-9** 位确定其他用户拥有该文件的权限

出现 `-` 就说明没有权限

### [](#B-3-命令用法 "B.3 命令用法")B.3 命令用法

#### [](#1-chown "1. chown")1\. chown

>
> chown \[–R\] 所有者名 文件名  
>  或      
> chown \[-R\] 属主名:属组名 文件名  
>


注：`[–R]` 为可加可不加，意思是递归更改文件属组，就是在更改某个目录文件的所有者（组）时，如果加上 **-R** 的参数，那么该目录下的所有文件的所有者（组）都会更改。

#### [](#2-chmod "2. chmod")2\. chmod

可以用数字或者字母设置文件权限，这里我们用最简单的方法——数字和

> **r=4，w=2，x=1**

每个用户的权限可以用以上数字和表示


| 权限数字和 | 权限解释 | rwx | 二进制 |
| --- | --- | --- | --- | 
| 7 | 读 \+ 写 \+ 执行 | rwx | 111 |
| 6 | 读 \+ 写 | rw- | 110 |
| 5 | 读 \+ 执行 | r-x | 101 |
| 4 | 只读 | r-- | 100 |
| 3 | 写 \+ 执行 | -wx | 011 |
| 2 | 只写 | -w- | 010 |
| 1 | 只执行 | --x | 001 |
| 0 | 无 | --- | 000 |



所以权限一般可以是：**777、665、775、444** 等等

例如， 765 这样解释：

>  **所有者**的权限用数字表达：所有者的权限数字的和。即 rwx ，也就是 4+2+1 ，应该是 7    
>   **用户组**的权限用数字表达：用户组的权限数字的和。即 rw- ，也就是 4+2+0 ，应该是 6    
>   **其它用户**的权限数字表达：其它用户权限数字的和。即 r-x ，也就是 4+0+1 ，应该是 5    

三个数字依次代表三个不同的用户组

命令正规表达式是
`
chmod 权限数字 文件名  
`

如，给文件 `test.xml` 777 权限

`
sudo chmod 777 test.xml  
`

如果要给文件夹下所有的文件权限，可以使用 ` -R ` 命令递归给予权限

`
sudo chmod -R 权限数字 目录  
`


[](#C-结尾 "C 结尾")C 结尾
--------------------

以上是学习笔记

> ### [](#参考文献 "参考文献")参考文献
> 
> 1\. [https://www.runoob.com/linux/linux-file-attr-permission.html](https://www.runoob.com/linux/linux-file-attr-permission.html)  
> 2\. [https://www.runoob.com/linux/linux-comm-chmod.html](https://www.runoob.com/linux/linux-comm-chmod.html)
	

[#技术](/tag/技术)   [#LINUX](/tag/LINUX)   [#学习](/tag/学习)
